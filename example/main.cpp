#ifdef DEBUG_ESP_PORT
#define BLYNK_PRINT DEBUG_ESP_PORT
#endif

#include <EspIotBlynk.h>

#if defined(ESP32)
#include <WiFi.h>
#include <BlynkSimpleEsp32_SSL.h>
#include <SPIFFS.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266_SSL.h>
#include <FS.h>

#define BLYNK_DEFAULT_ROOT_CA nullptr
#endif

// Globals
#ifdef WATCH_DOG_SUPPORT
static WatchDog watchdog(60000);
#endif

void setup()
{
	// Init
	DEBUG_ESP_BEGIN(115200);
	randomSeed(analogRead(39)); // Unconnected pin
	SPIFFS.begin();

	// Complete WiFi setup (note: 1=WPS)
	WiFiHomeSetup(ReadConfigEnumFromFS("WIFI_SETUP_MODE", WIFI_HOME_SETUP_WPS));

	// Init Time
	NtpInit(ReadConfigStringFromFS("NTP_SERVERS"), ReadConfigStringFromFS("TZ_INFO"));

	// Enable HTTP updates
	if (HttpUpdate.begin(HTTP_UPDATE_URL, ReadConfigStringFromFS("HTTP_UPDATE_CA")))
	{
		// Schedule update and randomize the interval by 30 seconds
		HttpUpdate.schedule(ReadConfigStringFromFS("HTTP_UPDATE_INTERVAL", "3600000").toInt() + random(30000));
	}

	// Configure Blynk (note: returned values are intentionally never deallocated)
	BlynkSetup(Blynk,
			   ReadConfigCharsFromFS("BLYNK_AUTH") ?: "",
			   ReadConfigCharsFromFS("BLYNK_DOMAIN") ?: BLYNK_DEFAULT_DOMAIN,
			   ReadConfigStringFromFS("BLYNK_PORT").toInt() ?: BLYNK_DEFAULT_PORT_SSL,
			   ReadConfigCharsFromFS("BLYNK_ROOT_CA") ?: BLYNK_DEFAULT_ROOT_CA);

	// SPIFFS no longer needed
	// Note: On ESP8266, HttpUpdate reopens SPIFFS during check
	SPIFFS.end();

	// Wait for time
	struct tm tm;
	getLocalTime(&tm, 10000);

	char timestr[40] = {0};
	strftime(timestr, sizeof(timestr), "%F %R:%S %z", &tm);
	DEBUG_ESP_LOG2(F("Current date: "), timestr);

// Setup complete
#ifdef WATCH_DOG_SUPPORT
	watchdog.begin();
#endif
	DEBUG_ESP_LOG1(F("Setup done"));
}

BLYNK_CONNECTED()
{
	DEBUG_ESP_LOG1(F("Blynk connected"));
	Blynk.syncAll();
}

void loop()
{
	Blynk.run();
#ifdef ESP8266
	HttpUpdate.run();
#endif

#ifdef WATCH_DOG_SUPPORT
	watchdog.heartbeat();
#endif
}
