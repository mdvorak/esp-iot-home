#pragma once
#include <time.h>

void NtpInit(const char *ntpServers, const char *tzInfo);

inline void NtpInit(const String &ntpServers, const String &tzInfo)
{
    NtpInit(ntpServers.c_str(), tzInfo.c_str());
}

#ifdef ESP8266
bool getLocalTime(struct tm * info, uint32_t ms);
#endif
