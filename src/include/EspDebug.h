#pragma once

#include <Arduino.h>

// Note: These are same as Blynk uses, but we have own copy to avoid Blynk dependency

#ifdef DEBUG_ESP_PORT
#define DEBUG_ESP_BEGIN(baud) { DEBUG_ESP_PORT.begin(baud); DEBUG_ESP_PORT.println(); }
#define DEBUG_ESP_LOG1(p1)          \
    {                               \
        DEBUG_ESP_LOG_TIME();       \
        DEBUG_ESP_PORT.println(p1); \
    }
#define DEBUG_ESP_LOG2(p1, p2)      \
    {                               \
        DEBUG_ESP_LOG_TIME();       \
        DEBUG_ESP_PORT.print(p1);   \
        DEBUG_ESP_PORT.println(p2); \
    }
#define DEBUG_ESP_LOG3(p1, p2, p3)  \
    {                               \
        DEBUG_ESP_LOG_TIME();       \
        DEBUG_ESP_PORT.print(p1);   \
        DEBUG_ESP_PORT.print(p2);   \
        DEBUG_ESP_PORT.println(p3); \
    }
#define DEBUG_ESP_LOG4(p1, p2, p3, p4) \
    {                                  \
        DEBUG_ESP_LOG_TIME();          \
        DEBUG_ESP_PORT.print(p1);      \
        DEBUG_ESP_PORT.print(p2);      \
        DEBUG_ESP_PORT.print(p3);      \
        DEBUG_ESP_PORT.println(p4);    \
    }
#define DEBUG_ESP_LOG5(p1, p2, p3, p4, p5) \
    {                                  \
        DEBUG_ESP_LOG_TIME();          \
        DEBUG_ESP_PORT.print(p1);      \
        DEBUG_ESP_PORT.print(p2);      \
        DEBUG_ESP_PORT.print(p3);      \
        DEBUG_ESP_PORT.print(p4);      \
        DEBUG_ESP_PORT.println(p5);    \
    }

static void DEBUG_ESP_LOG_TIME()
{
    DEBUG_ESP_PORT.print('[');
    DEBUG_ESP_PORT.print(millis());
    DEBUG_ESP_PORT.print(F("] "));
}
#else

#define DEBUG_ESP_BEGIN(baud)
#define DEBUG_ESP_LOG(...)
#define DEBUG_ESP_LOG1(p1)
#define DEBUG_ESP_LOG2(p1, p2)
#define DEBUG_ESP_LOG3(p1, p2, p3)
#define DEBUG_ESP_LOG4(p1, p2, p3, p4)
#define DEBUG_ESP_LOG5(p1, p2, p3, p4, p5)
#endif
