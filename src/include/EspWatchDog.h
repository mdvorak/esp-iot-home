#pragma once
#ifdef ESP32

#include <Arduino.h>
#include "EspDebug.h"

#define WATCH_DOG_SUPPORT 1

class WatchDog
{
  public:
    WatchDog(unsigned long failureMillis)
    {
        _max = failureMillis;
    }

    void heartbeat()
    {
        _last = millis();
    }

    void begin()
    {
        heartbeat();
        _enabled = true;
        xTaskCreate([](void *p) { static_cast<WatchDog *>(p)->task(); }, "WatchDog", 4096, this, 0, nullptr);
    }

    void suspend()
    {
        _enabled = false;
        DEBUG_ESP_LOG1("WatchDog suspended");
    }

    void resume()
    {
        heartbeat();
        _enabled = true;
        DEBUG_ESP_LOG1("WatchDog resumed");
    }

    void task()
    {
        DEBUG_ESP_LOG1(F("WatchDog started"));

        // Infinite loop
        for (;;)
        {
            // Wait
            vTaskDelay(1000 / portTICK_PERIOD_MS);

            // Check
            long duration = millis() - _last;
            if (_enabled && duration > _max)
            {
                // Failed, restart
                DEBUG_ESP_LOG3("ERROR: Loop WatchDog failed after ", duration, " ms, resetting");
                ESP.restart();
                return;
            }
        }
    }

  private:
    long _max = 0;
    volatile unsigned long _last = 0;
    volatile bool _enabled = false;
};

#endif
