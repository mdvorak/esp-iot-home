#pragma once

typedef enum
{
    WIFI_HOME_SETUP_NONE = 0,
    WIFI_HOME_SETUP_WPS
} wifi_home_setup_mode_t;

/**
 * Initializes WiFi, using either stored network or via WPS.
 * Performs initial connection if network is stored (blocking call), however do not handle connected state of the WiFi.
 * 
 * To handle connected/disconnected events, use following code:
 * 
 *  WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) { Blynk.disconnect(); }, SYSTEM_EVENT_WIFI_READY);
 *  WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) { Blynk.disconnect(); }, SYSTEM_EVENT_STA_DISCONNECTED);
 *  WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) { Blynk.connect(0); }, SYSTEM_EVENT_STA_GOT_IP);
 * 
 * Note that when using Blynk, BlynkSetup(...) or Blynk.config(...) should be called before this method as well.
 */
void WiFiHomeSetup(wifi_home_setup_mode_t mode);
