#pragma once

#include <Arduino.h>
#include "include/EspDebug.h"

#if defined(ESP32)
#include <WiFi.h>

template <typename B>
void BlynkSetup(B &blynk, const char *auth, const char *domain, uint16_t port, const char *root_ca)
{
    if (auth && strlen(auth))
    {
        // Configure
        blynk.config(auth, domain, port, root_ca);
        blynk.disconnect();

        // Connect when WiFi is available
        WiFi.onEvent([&blynk](WiFiEvent_t event, system_event_info_t info) { blynk.connect(0); }, SYSTEM_EVENT_STA_GOT_IP);
        WiFi.onEvent([&blynk](WiFiEvent_t event, system_event_info_t info) { blynk.disconnect(); }, SYSTEM_EVENT_WIFI_READY);
        WiFi.onEvent([&blynk](WiFiEvent_t event, system_event_info_t info) { blynk.disconnect(); }, SYSTEM_EVENT_STA_DISCONNECTED);

        // Connect if already connected
        if (WiFi.isConnected())
        {
            blynk.connect(0);
        }
    }
    else
    {
        // Disable auto reconnect
        DEBUG_ESP_LOG1("Blynk not configured!");
        blynk.disconnect();
    }
}

#elif defined(ESP8266)
#include <ESP8266WiFi.h>

template <typename B>
void BlynkSetup(B &blynk, const char *auth, const char *domain, uint16_t port, const char *fingerprint)
{
    if (auth && strlen(auth))
    {
        // Configure
        blynk.config(auth, domain, port, fingerprint);
        blynk.disconnect();

        // Connect when WiFi is available
        // Note: This method is supposed to be called only once, otherwise we would have to deal with handlers
        static auto onGotIpHandler = WiFi.onStationModeGotIP([&blynk](const WiFiEventStationModeGotIP &) { blynk.connect(0); });
        static auto onDisconnectedHandler = WiFi.onStationModeDisconnected([&blynk](const WiFiEventStationModeDisconnected &) { blynk.disconnect(); });

        // Connect if already connected
        if (WiFi.isConnected())
        {
            blynk.connect(0);
        }
    }
    else
    {
        // Disable auto reconnect
        DEBUG_ESP_LOG1("Blynk not configured!");
        blynk.disconnect();
    }
}

#endif
