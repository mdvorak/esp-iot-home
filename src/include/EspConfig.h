#pragma once

#include <Arduino.h>

// NOTE: Don't forget to SPIFFS.begin() before any calls
// NOTE: You can safely SPIFFS.end() after last value is read (unless you wan't to keep it open)

/**
 * Reads configuration from SPIFFS from file of given name.
 * 
 * Caller is responsible to delete[] returned array! (if desired)
 */
char *ReadConfigCharsFromFS(const char *name, const char *defaultValue = nullptr);

/**
 * Reads configuration from SPIFFS from file of given name.
 */
String ReadConfigStringFromFS(const char *name, const char *defaultValue = "");

/**
 * Reads configuration from SPIFFS from file of given name.
 * Value must be numeric representation of given enum.
 */
template <typename E>
E ReadConfigEnumFromFS(const char *name, E defaultValue = static_cast<E>(0))
{
    String value = ReadConfigStringFromFS(name);
    return value.length() ? (E)value.toInt() : defaultValue;
}
