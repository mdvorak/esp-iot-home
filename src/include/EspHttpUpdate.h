#pragma once

#include <Arduino.h>

#ifndef ESP8266
#include <HTTPClient.h>
#include <Update.h>
#include <Preferences.h>
#include <esp_task.h>
#else
#include <ESP8266HTTPClient.h>
#include <EEPROM.h>
#endif

class HttpUpdateClass
{
public:
  bool begin(const String &url, const String &caCert = "");

  void update();

  void schedule(uint32_t interval);

#ifdef ESP8266
  void run();
#endif

private:
  String _url;
  String _caCert;
  bool _running = false;
  uint32_t _interval = 0;

#ifndef ESP8266
  TaskHandle_t _handle = 0;
  void task();
#else
  uint32_t _lastCheck = 0;
#endif

  void doUpdate(const String &url, int maxRedirects = 5);
  String readLastModified();
  void writeLastModified(String lastModified);
};

extern HttpUpdateClass HttpUpdate;
