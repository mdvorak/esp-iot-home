#ifdef ESP8266
#include <ESP8266WiFi.h>
#else
#include <WiFi.h>
#endif

#include "include/EspTime.h"
#include "include/EspConfig.h"
#include "include/EspDebug.h"

static char *trim_end(char *str);

void NtpInit(const char *ntpServers, const char *tzInfo)
{
    // Defaults
    if (!ntpServers || strlen(ntpServers) == 0)
    {
        ntpServers = "0.pool.ntp.org\n1.pool.ntp.org\n2.pool.ntp.org";
    }
    if (!tzInfo || strlen(tzInfo) == 0)
    {
        tzInfo = "UTC";
    }

    // Create string copy
    char *ntpServersStr = new char[strlen(ntpServers) + 1];
    strcpy(ntpServersStr, ntpServers);

    // Note: Never deallocate these strings, time api might use them as constants during runtime,
    // and this method is supposed to be called only once during setup
    char *ptr = nullptr;
    char *ntpServer1 = trim_end(strtok_r(ntpServersStr, "\n", &ptr));
    char *ntpServer2 = trim_end(strtok_r(nullptr, "\n", &ptr));
    char *ntpServer3 = trim_end(strtok_r(nullptr, "\n", &ptr));

    DEBUG_ESP_LOG3(F("NTP Server 1: '"), ntpServer1, "'");
    DEBUG_ESP_LOG3(F("NTP Server 2: '"), ntpServer2, "'");
    DEBUG_ESP_LOG3(F("NTP Server 3: '"), ntpServer3, "'");
    DEBUG_ESP_LOG3(F("TZ: '"), tzInfo, "'");

#ifdef ESP8266
    configTime(0, 0, ntpServer1, ntpServer2, ntpServer3);
    setenv("TZ", tzInfo, 1);
    tzset();
#else
    configTzTime(tzInfo, ntpServer1, ntpServer2, ntpServer3);
#endif

    // If already connected, no need for handlers
    if (WiFi.isConnected())
    {
        struct tm tm;
        getLocalTime(&tm, 0);
        return;
    }

    // Call getLocalTime once to start NTP sync
#ifdef ESP8266
    static auto onGotIpHandler = WiFi.onStationModeGotIP([](const WiFiEventStationModeGotIP &) {
        struct tm tm;
        getLocalTime(&tm, 0);
    });
    (void)onGotIpHandler;
#else
    WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) {
        struct tm tm;
        getLocalTime(&tm, 0);
    },
                 SYSTEM_EVENT_STA_GOT_IP);
#endif
}

static char *trim_end(char *str)
{
    if (str != nullptr)
    {
        // Remove trailing whitespaces
        for (size_t last = strlen(str) - 1; last >= 0 && isspace(str[last]); --last)
        {
            str[last] = '\0';
        }
    }
    return str;
}

#ifdef ESP8266

// Polyfill for ESP8266
bool getLocalTime(struct tm *info, uint32_t ms)
{
    uint32_t count = ms / 10;
    time_t now;

    time(&now);
    localtime_r(&now, info);

    if (info->tm_year > (2016 - 1900))
    {
        return true;
    }

    while (count--)
    {
        delay(10);
        time(&now);
        localtime_r(&now, info);
        if (info->tm_year > (2016 - 1900))
        {
            return true;
        }
    }
    return false;
}

#endif
