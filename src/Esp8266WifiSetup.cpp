#ifdef ESP8266

#include <ESP8266WiFi.h>
#include "include/EspWifiSetup.h"
#include "include/EspDebug.h"

static bool wifi_is_ssid_stored()
{
  struct station_config conf = {0};
  wifi_station_get_config(&conf);
  return conf.ssid[0] != '\0';
}

static bool wifi_wait_for_connect(unsigned long timeout)
{
  auto start = millis();
  while (millis() - start < timeout)
  {
    if (WiFi.status() == WL_CONNECTED || WiFi.status() == WL_CONNECT_FAILED)
    {
      break;
    }
    delay(200);
#ifdef DEBUG_ESP_PORT
    DEBUG_ESP_PORT.print(WiFi.status());
#endif
  }

#ifdef DEBUG_ESP_PORT
  DEBUG_ESP_PORT.println();
#endif

  return WiFi.status() == WL_CONNECTED;
}

void WiFiHomeSetup(wifi_home_setup_mode_t mode)
{
  DEBUG_ESP_LOG2(F("WiFi setup mode "), mode);

  // Disable automatic connection - causes issues
  if (WiFi.getAutoConnect())
    WiFi.setAutoConnect(false);
  // Enable automatic reconnection - it is enough for ESP8266 and doesn't need extra code
  if (!WiFi.getAutoReconnect())
    WiFi.setAutoReconnect(true);

  // Configure
  WiFi.persistent(true);
  WiFi.enableSTA(true);

  // On Connected
  static auto connectedHandler = WiFi.onStationModeConnected([](const WiFiEventStationModeConnected &) { DEBUG_ESP_LOG2(F("WiFi connected to "), WiFi.SSID()); });
  (void)connectedHandler;

  // On Got IP
  static auto gotIpHandler = WiFi.onStationModeGotIP([](const WiFiEventStationModeGotIP &) { DEBUG_ESP_LOG2(F("WiFi got IP "), WiFi.localIP()); });
  (void)gotIpHandler;

  // On Disconnected
  static auto disconnectedHandler = WiFi.onStationModeDisconnected([](const WiFiEventStationModeDisconnected &) { DEBUG_ESP_LOG1(F("WiFi disconnected")); });
  (void)disconnectedHandler;

  // Is configuration stored?
  if (wifi_is_ssid_stored())
  {
    // Try connect
    DEBUG_ESP_LOG1(F("WiFi trying initial WiFi connection"));
    WiFi.begin();

    // Wait for connection
    auto connected = wifi_wait_for_connect(15000);
    DEBUG_ESP_LOG1(connected ? F("WiFi connected") : F("WiFi failed"));
  }
  else
  {
    DEBUG_ESP_LOG1(F("WiFi no network stored"));
  }

  // If not connected
  if (!WiFi.isConnected())
  {
    // WPS support - only once after HW reset
    auto resetReason = ESP.getResetInfoPtr()->reason;

    if (mode != WIFI_HOME_SETUP_WPS)
    {
      DEBUG_ESP_LOG1(F("WiFi WPS disabled by configuration"));
    }
    // Never use WPS when waking up from deep sleep etc
    else if (resetReason == REASON_DEFAULT_RST || resetReason == REASON_EXT_SYS_RST)
    {
      DEBUG_ESP_LOG1(F("WiFi starting WPS"));

      // WPS - ESP8266 has short timeout, so retry for one minute
      auto wpsStart = millis();
      while (WiFi.beginWPSConfig() && !wifi_is_ssid_stored() && millis() - wpsStart < 60000)
      {
        DEBUG_ESP_LOG1(F("WiFi WPS timeout"));
      }

      // Log result
      DEBUG_ESP_LOG1(wifi_is_ssid_stored() ? F("WiFi WPS succeeded") : F("WiFi WPS failed"));
    }
    else
    {
      // Don't start WPS
      DEBUG_ESP_LOG3(F("WiFi reset reason "), resetReason, F(", WPS disabled"));
    }
  }

  // Initialized
  DEBUG_ESP_LOG1(F("WiFi init completed"));
}

#endif
