#include "include/EspConfig.h"
#include "include/EspDebug.h"

#ifndef ESP8266
#include <SPIFFS.h>
#else
#include <FS.h>
#endif

char *ReadConfigCharsFromFS(const char *name, const char *defaultValue)
{
    File file = SPIFFS.open(String("/") + name, "r");
    if (!file || file.size() == 0)
    {
        // Does not exist
        DEBUG_ESP_LOG2(F("Config not found: "), name);
        file.close();

        // Copy default value into allocated buffer
        if (defaultValue != nullptr)
        {
            auto size = strlen(defaultValue) + 1; // include terminal character
            char *value = new char[size];
            memcpy(value, defaultValue, size);
            return value;
        }

        // No default
        return nullptr;
    }

    // Read value into allocated buffer
    char *value = new char[file.size() + 1];
    auto read = file.readBytes(value, file.size());
    value[read] = '\0'; // Make sure we have terminal character
    file.close();

    // Trim ending whitespace
    while (read > 0)
    {
        if (isspace(--read))
        {
            value[read] = '\0';
        }
        else
        {
            break;
        }
    }

    // Return
    DEBUG_ESP_LOG4(F("Config "), name, '=', value);
    return value;
}

String ReadConfigStringFromFS(const char *name, const char *defaultValue)
{
    char *data = ReadConfigCharsFromFS(name);
    String value(data != nullptr && data[0] != '\0' ? data : defaultValue);

    delete[] data;
    return value;
}