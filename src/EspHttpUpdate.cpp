#include "include/EspHttpUpdate.h"
#include "include/EspDebug.h"

#ifdef ESP8266
#include <WiFiClientSecure.h>
#include <FS.h>
const char *SPIFFS_FILE_NAME = "/$httpupdate";
#endif

const char *HTTP_SCHEME = "http://";
const char *HTTPS_SCHEME = "https://";
const char *CONTENT_LENGTH_HEADER = "Content-Length: ";
const char *CONTENT_TYPE_HEADER = "Content-Type: ";

// Global instance
HttpUpdateClass HttpUpdate;

// Custom ESP8266 client class
#ifdef ESP8266
class CustomHTTPClient : public HTTPClient
{
public:
    bool beginCACert(String url, const String& caCert);
};
#endif

// HttpUpdateClass implementation
bool HttpUpdateClass::begin(const String &url, const String &caCert)
{
#if defined(DEBUG_ESP_PORT) && !defined(ESP8266)
    // Note: Making static variable causes this init to be called only once, ever
    static auto &progressNotify = Update.onProgress([](size_t current, size_t total) {
        DEBUG_ESP_LOG5("OTA Update ", current / 1024, '/', total / 1024, " KB");
    });
    (void)progressNotify; // Suppress unused variable warning
#endif

    // Disabled
    if (!url.length())
    {
        DEBUG_ESP_LOG1(F("OTA URL not configured"));
        return false;
    }

    _url = url;
    _caCert = caCert;

    DEBUG_ESP_LOG2(F("OTA URL: "), _url);
    DEBUG_ESP_LOG2(F("OTA Last-Modified: "), readLastModified());
    return true;
}

void HttpUpdateClass::update()
{
    if (_running)
    {
        DEBUG_ESP_LOG1(F("OTA Update already in progress"));
        return;
    }

    _running = true;
    doUpdate(_url);
    _running = false;
}

void HttpUpdateClass::doUpdate(const String &url, int maxRedirects)
{
    // Connect
    DEBUG_ESP_LOG2(F("OTA Downloading from "), url);
    bool ssl = url.startsWith("https:");

#ifndef ESP8266
    HTTPClient client;
    client.setTimeout(10000);
    bool clientStarted = ssl ? client.begin(url, _caCert.c_str()) : client.begin(url);
#else
    // Custom SSL CA Cert support
    CustomHTTPClient client;
    client.setTimeout(10000);
    bool clientStarted = ssl ? client.beginCACert(url, _caCert.c_str()) : client.begin(url);
#endif

    if (!clientStarted)
    {
        DEBUG_ESP_LOG1(F("OTA Failed to begin connection"));
        return;
    }

    // Request Headers
    client.addHeader("Accept", "application/octet-stream");

    String ifModifiedSince = readLastModified();
    if (ifModifiedSince.length())
    {
        DEBUG_ESP_LOG2(F("OTA If-Modified-Since: "), ifModifiedSince);
        client.addHeader("If-Modified-Since", ifModifiedSince);
    }

    // Setup response headers
    const char *HEADERS[] = {"Content-Type", "Location", "Last-Modified"};
    client.collectHeaders(HEADERS, 3);

    // Send Request
    int statusCode = client.GET();
    DEBUG_ESP_LOG2(F("OTA Response "), statusCode);

    // Handle response
    if (statusCode == 304)
    {
        // Not modified, do nothing
        DEBUG_ESP_LOG1(F("OTA Firmware is up-to-date"));
        return;
    }
    if (statusCode >= 301 && statusCode <= 308 && statusCode != 305)
    {
        String location = client.header(1u);
        if (!location.length())
        {
            DEBUG_ESP_LOG1(F("OTA Redirect without Location header!"));
            return;
        }

        DEBUG_ESP_LOG1(F("OTA Redirecting"));

        // Maximum redirects reached
        if (maxRedirects <= 0)
        {
            DEBUG_ESP_LOG1(F("OTA Maximum redirect count reached, update failed"));
            return;
        }

        // Handle relative redirect
        if (location.startsWith("/"))
        {
            int schemeEnd = url.indexOf("//");
            int hostEnd = schemeEnd > 0 ? url.indexOf('/', schemeEnd + 2) : -1;
            location = url.substring(0, hostEnd >= 0 ? hostEnd : url.length()) + location;
        }

        // Close currect connection explicitly and use recursive call
        client.end();
        doUpdate(location, maxRedirects - 1);
        return;
    }
    if (statusCode < 200 || statusCode >= 300)
    {
        // Failure
        DEBUG_ESP_LOG3(F("OTA Request failed ("), statusCode, ')');
        return;
    }

    // Validate type
    if (client.header(0u) != "application/octet-stream")
    {

        DEBUG_ESP_LOG2(F("OTA Invalid Content-Type received: "), client.header(0u));
        return;
    }

    String lastModified = client.header(2u);
    if (!lastModified.length())
    {
        DEBUG_ESP_LOG1(F("OTA No Last-Modified received, this would lead to infinite update loop! Aborting"));
        return;
    }
    DEBUG_ESP_LOG2(F("OTA Last-Modified: "), lastModified);

    // Manually check for modification timestamp
    if (lastModified == ifModifiedSince)
    {
        DEBUG_ESP_LOG1(F("OTA Firmware is up-to-date (server ignored If-Modified-Since)"));
        return;
    }

    // Validate size
    if (client.getSize() <= 0)
    {
        DEBUG_ESP_LOG1(F("OTA Update failed, either response has 0 bytes or Content-Length header is missing"));
        return;
    }

    // Start update
    if (!Update.begin(client.getSize()))
    {
        DEBUG_ESP_LOG1(F("OTA Update failed, not enough space"));
        return;
    }

    // Perform update
    DEBUG_ESP_LOG3(F("OTA Update started ("), client.getSize(), F(" bytes)"));
    auto written = Update.writeStream(*client.getStreamPtr());
    DEBUG_ESP_LOG5(F("OTA Update written "), written, " of ", client.getSize(), F(" bytes"));

    // Finish update
    if (Update.end())
    {
        // Success
        client.end();
        writeLastModified(lastModified);

        // Restart
        DEBUG_ESP_LOG1(F("OTA Update completed successfully, restarting..."));
        ESP.restart();
    }
    else
    {
        // Failed to write the update
        DEBUG_ESP_LOG2(F("OTA Update failed: "), Update.getError())
    }
}

void HttpUpdateClass::schedule(uint32_t interval)
{
    if (interval > 0)
        _interval = interval < 30000 ? 30000 : interval;
    else
        _interval = 0;

    if (_interval > 0)
    {
        DEBUG_ESP_LOG3(F("OTA Updates scheduled every "), interval, F(" ms"));

#ifndef ESP8266
        if (!_handle)
        {
            xTaskCreate([](void *p) { static_cast<HttpUpdateClass *>(p)->task(); }, "Esp32HttpUpdate", 8192, this, tskIDLE_PRIORITY, &_handle);
        }
#endif
    }
    else
    {
        DEBUG_ESP_LOG1(F("OTA Updates disabled"));

#ifndef ESP8266
        if (_handle)
        {
            vTaskDelete(_handle);
            _handle = nullptr;
        }
#endif
    }
}

#ifndef ESP8266
void HttpUpdateClass::task()
{
    for (;;)
    {
        // Sleep
        delay(_interval);
        // Try update
        update();
    }
}
#else
void HttpUpdateClass::run()
{
    // Check interval
    if (_interval > 0 && millis() - _lastCheck > _interval)
    {
        // Try update
        _lastCheck = millis();
        update();
    }
}
#endif

String HttpUpdateClass::readLastModified()
{
#ifndef ESP8266
    Preferences pref;
    pref.begin("httpupdate");
    return pref.getString("Last-Modified", "");
#else
    if (!SPIFFS.begin())
    {
        DEBUG_ESP_LOG1(F("OTA Failed to read Last-Modified, SPIFFS cannot be mounted"));
        return String();
    }

    auto file = SPIFFS.open(SPIFFS_FILE_NAME, "r");
    if (file)
    {
        String lastModified = file.readString();
        file.close();

        lastModified.trim();
        return lastModified;
    }

    file.close();
    return String();
#endif
}

void HttpUpdateClass::writeLastModified(String lastModified)
{
#ifndef ESP8266
    Preferences pref;
    pref.begin("httpupdate");
    if (!pref.putString("Last-Modified", lastModified))
    {
        DEBUG_ESP_LOG1(F("OTA Failed to write Last-Modified to preferences!"));
    }
    else
    {
        DEBUG_ESP_LOG2(F("OTA Written Last-Modified "), lastModified);
    }
#else
    if (!SPIFFS.begin())
    {
        DEBUG_ESP_LOG1(F("OTA Failed to write Last-Modified, SPIFFS cannot be mounted"));
        return;
    }

    auto file = SPIFFS.open(SPIFFS_FILE_NAME, "w");
    if (!file)
    {
        DEBUG_ESP_LOG1(F("OTA Failed to write Last-Modified, SPIFFS file cannot be opened"));
        file.close();
        return;
    }

    file.print(lastModified);
    file.close();
#endif
}


#ifdef ESP8266

// HACK: Class header redefinition, needed for custom implementation
class TransportTraits
{
public:
    virtual ~TransportTraits();
    virtual std::unique_ptr<WiFiClient> create();
    virtual bool verify(WiFiClient& client, const char* host);
};

// Implementation using CACert instead of fingerprint
class BearSSLCertTraits : public TransportTraits
{
public:
    BearSSLCertTraits(const String& caCert) : _caCert(caCert)
    {
    }

    std::unique_ptr<WiFiClient> create() override
    {
        BearSSL::WiFiClientSecure *client = new BearSSL::WiFiClientSecure();
        client->setCACert(reinterpret_cast<const uint8_t*>(_caCert.c_str()), _caCert.length());
        return std::unique_ptr<WiFiClient>(client);
    }

    bool verify(WiFiClient& client, const char* host) override
    {
        // No-op.  BearSSL will not connect if the ca cert doesn't match.
        // So if you get to here you've already connected and it matched
        (void) client;
        (void) host;
        return true;
    }

protected:
    String _caCert;
};

bool CustomHTTPClient::beginCACert(String url, const String& caCert)
{
    _transportTraits.reset(nullptr);
    _port = 443;
    if (!beginInternal(url, "https"))
    {
        return false;
    }
    _transportTraits = TransportTraitsPtr(new BearSSLCertTraits(caCert));
    return true;
}

#endif
