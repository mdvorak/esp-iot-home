#ifdef ESP32

#include <esp_wifi.h>
#include <esp_wps.h>
#include <rom/rtc.h>
#include <WiFi.h>
#include "include/EspWifiSetup.h"
#include "include/EspDebug.h"

static volatile bool wifiAutoReconnect = false;

static void wps_config_init(esp_wps_config_t &config, wps_type_t type)
{
  config.wps_type = type;
  config.crypto_funcs = &g_wifi_default_wps_crypto_funcs;
  strcpy_P(config.factory_info.manufacturer, reinterpret_cast<PGM_P>(F("ESPRESSIF")));
  strcpy_P(config.factory_info.model_number, reinterpret_cast<PGM_P>(F("ESP32")));
  strcpy_P(config.factory_info.model_name, reinterpret_cast<PGM_P>(F("ESPRESSIF IOT")));
  strcpy_P(config.factory_info.device_name, reinterpret_cast<PGM_P>(F("ESP STATION")));
}

static bool wifi_is_ssid_stored()
{
  wifi_config_t conf;
  esp_wifi_get_config(WIFI_IF_STA, &conf);
  return conf.sta.ssid[0] != '\0';
}

static bool wifi_wait_for_connect(unsigned long timeout)
{
  auto start = millis();
  while (millis() - start < timeout)
  {
    if (WiFi.status() == WL_CONNECTED || WiFi.status() == WL_CONNECT_FAILED)
    {
      break;
    }
    delay(200);
#ifdef DEBUG_ESP_PORT
    DEBUG_ESP_PORT.print('.');
#endif
  }

#ifdef DEBUG_ESP_PORT
  DEBUG_ESP_PORT.println();
#endif

  return WiFi.status() == WL_CONNECTED;
}

static void task_maintain_wifi(void *)
{
  // Timer support
  const uint8_t DELAYS[] = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233};
  uint8_t failures = 0;

  // Infinite task loop
  for (;;)
  {
    if (wifiAutoReconnect && !WiFi.isConnected() && wifi_is_ssid_stored())
    {
      // Simple back-off algorithm
      TickType_t waitFor = DELAYS[failures] * 1000;
      failures = _min(sizeof(DELAYS) / sizeof(uint8_t), failures + 1);

      // Delay
      DEBUG_ESP_LOG3(F("Wifi reconnect - waiting approx "), waitFor, " ms");
      vTaskDelay(waitFor / portTICK_PERIOD_MS);

      // Start reconnect
      DEBUG_ESP_LOG1(F("WiFi auto reconnecting"));
      WiFi.begin();

      // Wait for connection
      vTaskDelay(15000 / portTICK_PERIOD_MS);

      // Reset failures
      if (WiFi.isConnected())
      {
        failures = 0;
      }
    }
    else
    {
      // Simply wait for a moment
      vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
  }
}

void WiFiHomeSetup(wifi_home_setup_mode_t mode)
{
  DEBUG_ESP_LOG2(F("WiFi setup mode "), mode);

  // Disable automatic connection - causes issues
  if (WiFi.getAutoConnect())
    WiFi.setAutoConnect(false);
  // Disable automatic reconnection - we have custom task with exponential backoff, which also works well with WPS
  if (WiFi.getAutoReconnect())
    WiFi.setAutoReconnect(false);

  // Configure
  WiFi.persistent(true);
  WiFi.enableSTA(true);

  // Create WiFi maintenance task
  xTaskCreate(task_maintain_wifi, "MaintainWiFi", 1024, nullptr, tskIDLE_PRIORITY + 1, nullptr);

  // On Ready (also means "trying to connect")
  WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) { DEBUG_ESP_LOG1(F("WiFi ready")); }, SYSTEM_EVENT_WIFI_READY);

  // On Connected
  WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) { DEBUG_ESP_LOG2(F("WiFi connected to "), WiFi.SSID()); }, SYSTEM_EVENT_STA_CONNECTED);

  // On Got IP
  WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) { DEBUG_ESP_LOG2(F("WiFi got IP "), WiFi.localIP()); wifiAutoReconnect = true; }, SYSTEM_EVENT_STA_GOT_IP);

  // On Lost IP
  WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) { DEBUG_ESP_LOG1(F("WiFi lost IP")); }, SYSTEM_EVENT_STA_LOST_IP);

  // On Disconnected
  WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) { DEBUG_ESP_LOG1(F("WiFi disconnected")); }, SYSTEM_EVENT_STA_DISCONNECTED);

  // Is configuration stored?
  if (wifi_is_ssid_stored())
  {
    // Try connect
    DEBUG_ESP_LOG1(F("WiFi trying initial WiFi connection"));
    WiFi.begin();

    // Wait for connection
    auto connected = wifi_wait_for_connect(15000);
    DEBUG_ESP_LOG1(connected ? F("WiFi connected") : F("WiFi failed"));
  }
  else
  {
    DEBUG_ESP_LOG1(F("WiFi no network stored"));
  }

  // WPS support - only once after HW reset
  bool wpsStarted = false;
  if (!WiFi.isConnected())
  {
    if (mode != WIFI_HOME_SETUP_WPS)
    {
      DEBUG_ESP_LOG1(F("WiFi WPS disabled by configuration"));
    }
    // Never use WPS when waking up from deep sleep etc
    else if (rtc_get_reset_reason(0) == POWERON_RESET)
    {
      DEBUG_ESP_LOG1(F("WiFi starting WPS"));

      // WPS Successful
      WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) {
        DEBUG_ESP_LOG1(F("WiFi WPS successful")); esp_wifi_wps_disable(); wifiAutoReconnect = true; }, SYSTEM_EVENT_STA_WPS_ER_SUCCESS);

      // WPS Failed
      WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) {
        esp_wifi_wps_disable();
        DEBUG_ESP_LOG1(F("WiFi WPS failed, no retry till HW reset"));
        wifiAutoReconnect = wifi_is_ssid_stored();
        DEBUG_ESP_LOG1(wifiAutoReconnect ? F("WiFi trying to connect to stored network") : F("WiFi no network stored, disabled"))
      },
                   SYSTEM_EVENT_STA_WPS_ER_FAILED);

      // WPS Timeout
      WiFi.onEvent([](WiFiEvent_t event, system_event_info_t info) {
        esp_wifi_wps_disable();
        DEBUG_ESP_LOG1(F("WiFi WPS timeout, no retry till HW reset"));
        wifiAutoReconnect = wifi_is_ssid_stored();
        DEBUG_ESP_LOG1(wifiAutoReconnect ? F("WiFi trying to connect to stored network") : F("WiFi no network stored, disabled"))
      },
                   SYSTEM_EVENT_STA_WPS_ER_TIMEOUT);

      // Initialize WPS
      esp_wps_config_t wps;
      wps_config_init(wps, WPS_TYPE_PBC);

      // Start WPS
      esp_wifi_wps_enable(&wps);
      esp_wifi_wps_start(2 * 60000); // 2 min
      wpsStarted = true;
    }
    else
    {
      // Don't start WPS
      DEBUG_ESP_LOG3(F("WiFi reset reason "), rtc_get_reset_reason(0), F(", WPS disabled"));
    }
  }

  // Initialized
  wifiAutoReconnect = !wpsStarted;
  DEBUG_ESP_LOG1(F("WiFi init completed"));
}

#endif
